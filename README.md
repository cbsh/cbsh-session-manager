# CBSH Logout Application (Session Manager)
#### A Simple session manager application for CBSH

![](preview.png)

Images are from [DT's Logout Application he created for a YT video](https://gitlab.com/dwt1/byebye)

# Installation

The Session Manager can be installed two ways:

### CBSH Repos (Arch Linux)
if you're running a new install of CBSH, the Session manager will already be installed, for older installs before the Session Manager was added, you can download and install it from the repos, by running `sudo pacman -S cbsh-session-manager`

if you would like to add the Repo's to access this, add my repo to your `/etc/pacman.conf`:
```
[cbsh-arch-repo]
SigLevel = Required DatabaseOptional
Server https://gitlab.com/cbsh/cbsh-arch/$repo/-/raw/main/$arch
```
Then Sync your repositories with `sudo pacman -Syyu`


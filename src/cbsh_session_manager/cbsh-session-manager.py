#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Gio, GLib

import os
from os.path import exists, expanduser
from pathlib import Path

import tomlkit
from tomlkit import document, comment, nl, table
import shutil
import subprocess

home = expanduser("~")
settings = home + "/.config/cbsh-session-manager/settings.toml"
builder = Gtk.Builder()
builder.add_from_file("cbsh-logout.glade")
logoutDialog = builder.get_object("LogoutDialog")
restartDialog = builder.get_object("RestartDialog")
shutdownDialog = builder.get_object("ShutdownDialog")
colourSchemeCombo = builder.get_object("colourSchemeCombo")
colourThemesStore = builder.get_object("colourThemesStore")
lockerEntry = builder.get_object("lockerEntry")

#Make Settings Dir if it doesn't exist
if not os.path.exists(home + "/.config/cbsh-session-manager"):
    os.makedirs(home + "/.config/cbsh-session-manager")

# Make Settings TOML File if it doesn't exist
if not os.path.exists(settings):
    defaultSettings = document()
    defaultSettings.add(comment("Below are the Default Settings for CBSH Session Manager."))
    defaultSettings.add("color_theme", "cbsh-vibrant")
    defaultSettings.add("locker", "betterlockscreen")
    with open(settings, mode="wt", encoding="utf-8") as fp:
        tomlkit.dump(defaultSettings, fp)

with open(settings, mode="rt", encoding="utf-8") as fp:
    config = tomlkit.load(fp)

# Print to STDOUT what the current settings are
print("Current Colour Theme: " + config["color_theme"])
print("Current Locker: " + config["locker"])

# Function to change colour theme based on given input
themeProvider = Gtk.CssProvider()
themeProvider.load_from_file(Gio.File.new_for_path(f"themes/{config['color_theme']}.css"))
ctx = Gtk.StyleContext()
ctx.add_provider_for_screen(Gdk.Screen.get_default(), themeProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

class Handler():
    def on_settings_button_clicked(self, button):
        mainWin.hide()
        settingsWin.show_all()

    def on_cancel_button_clicked(self, button):
        print("Exiting, User Chose: Cancel")
        Gtk.main_quit()

    def on_logout_button_clicked(self, button):
        print('User Chose: Logout')
        logoutDialog.show_all()
    def on_confirmLogoutBtn_clicked(self, button):
        subprocess.run(["qtile", "cmd-obj", "-o", "cmd", "-f", "shutdown"])
    def on_cancelLogoutBtn_clicked(self, button):
        logoutDialog.hide()

    def on_shutdown_button_clicked(self, button):
        print('User Chose: Shut Down')
        shutdownDialog.show_all()
    def on_confirmShutdownBtn_clicked(self, button):
        subprocess.run(["systemctl", "poweroff"])
    def on_cancelShutdownBtn_clicked(self, button):
        shutdownDialog.hide()

    def on_restart_button_clicked(self, button):
        print('User Chose: Restart')
        restartDialog.show_all()
    def on_confirmRestartBtn_clicked(self, button):
        subprocess.run(["systemctl", "reboot"])
    def on_cancelRestartBtn_clicked(self, button):
        restartDialog.hide()


    def on_lock_screen_button_clicked(self, button):
        print('User Chose: Lock Screen')
        if config["locker"] == "i3lock":
            subprocess.run(["i3lock"])
        if config["locker"] == "slock":
            subprocess.run(["slock"])
        if config["locker"] == "betterlockscreen":
            subprocess.run(["betterlockscreen", "-l"])
        Gtk.main_quit()

    def on_homeBtn_clicked(self, button):
       settingsWin.hide()
       mainWin.show_all()

    def on_colourSchemeCombo_changed(self, button):
        print("Do Something Here.")

    lockerEntry.set_placeholder_text(config["locker"])
    lockerEntry.set_text(config["locker"])

    global colourThemes
    colourThemes = ["cbsh-vibrant",
                    "cbsh-black-hole",
                    "doom-one",
                    "doom-dracula",
                    "doom-solarized-dark"]
    colourSchemeCombo.set_id_column(0)
    for colourTheme in colourThemes:
        if colourTheme == config["color_theme"]:
            colourSchemeCombo.set_active(colourThemes.index(colourTheme))

    def on_saveSettingsBtn_clicked(self, button):
        activeTheme = colourSchemeCombo.get_active_id()
        activeLocker = lockerEntry.get_text()
        config["color_theme"] = activeTheme
        config["locker"] = activeLocker
        with open(settings, mode="wt", encoding="utf-8") as fp:
            tomlkit.dump(config, fp)

builder.connect_signals(Handler())
mainWin = builder.get_object("MainWindow")
settingsWin = builder.get_object("SettingsWindow")
mainWin.connect("destroy", Gtk.main_quit)
settingsWin.connect("destroy", Gtk.main_quit)

mainWin.show_all()
Gtk.main()
